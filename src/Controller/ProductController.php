<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductController extends Controller
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $entityManager  = $this->getDoctrine()->getManager();
        $product  = new Product();
        $product->setName('Apple');
        $product->setPrice(100);
        $product->setDescription('100 RS per Kg.');
        $entityManager->persist($product);
        $entityManager->flush();
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'new_product_id' => $product->getId()
        ]);
    }

    /**
     * @Route("product/{id}",name="product_show")
    */
    public function show($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class);
        $this_product = $product->find($id);
        $all_products = $product->findAll();
        return $this->render('product/index.html.twig',[
            'controller_name'=>'ProductController',
            'product' => $this_product,
            'all_products' => $all_products,
            'new_product_id' => ''
        ]);
    }
}
